'use strict';

import * as vscode from 'vscode';
import * as path from 'path';
import * as fs from 'fs';
import * as findUpGlob from 'find-up-glob';
import * as handlebars from 'handlebars';
import * as uuid from 'uuid/v4';


export function activate(context: vscode.ExtensionContext) {

    context.subscriptions.push(vscode.commands.registerCommand('csharp-new-file.createClass', createType.bind(null, { })));
    context.subscriptions.push(vscode.commands.registerCommand('csharp-new-file.createInterface', createType.bind(null, { type: 'Interface', prefix: 'I' })));
    context.subscriptions.push(vscode.commands.registerCommand('csharp-new-file.createEnum', createType.bind(null, { type: 'Enum' })));
    context.subscriptions.push(vscode.commands.registerCommand('csharp-new-file.createRecord', createType.bind(null, { type: 'Record' })));
    context.subscriptions.push(vscode.commands.registerCommand('csharp-new-file.createStruct', createType.bind(null, { type: 'Struct' })));
}

export function deactivate() {
}

interface CreateFileOptions {
    type?: string;
    prefix?: string;
    suffix?: string;
}

function createType(options: CreateFileOptions, args: any) {
    
    let { type = 'Class', prefix = '', suffix = '.cs' } = options;
    let defaultName = `${prefix}New${type}${suffix}`;

    vscode.window
        .showInputBox({
            ignoreFocusOut: true,
            prompt: 'Please enter filename',
            value: defaultName
        })
        .then(newFileName => {

            if (typeof newFileName === 'undefined') {
                return;
            }

            if (prefix && !newFileName.startsWith(prefix)) {
                newFileName = prefix + newFileName;
            }

            if (suffix && !newFileName.endsWith(suffix)) {
                newFileName = newFileName + suffix;
            }

            let workingDirectory =
                (args && args.fsPath) ||
                (vscode.window.activeTextEditor && vscode.window.activeTextEditor.document.fileName) ||
                vscode.workspace.rootPath;
            
            if (!fs.lstatSync(workingDirectory).isDirectory()) {
                workingDirectory = path.dirname(workingDirectory);
            }

            let newFilePath = path.join(workingDirectory, newFileName);

            if (fs.existsSync(newFilePath)) {
                throw new Error('File already exists');
            }

            let projectFilePath = getProjectFilePath(workingDirectory);
            let projectRootDir = removeTrailingSeparator(path.dirname(projectFilePath));

            let rootNamespace =
                getPropertyFromProjectFile(projectFilePath, 'RootNamespace') ||
                getPropertyFromProjectFile(projectFilePath, 'AssemblyName') ||
                path.basename(projectFilePath, '.csproj');

            let namespace =     
                rootNamespace +
                newFilePath.substring(projectRootDir.length, newFilePath.lastIndexOf(path.sep))
                    .replace(new RegExp(`\\${path.sep}`, 'g'), '.')
                    .replace(/\s+/g, "_")
                    .replace(/-/g, "_");

            let typeName = path.basename(newFilePath, suffix);

            openTemplateAndSaveNewFile(`${defaultName}.template`, { namespace, typeName }, newFilePath);
        })
        .then(undefined, (error: Error) => {
            vscode.window.showErrorMessage(`Error running command: ${error && error.message}`);
        });
}

function getProjectFilePath(cwd: string) {
    let csprojFiles = findUpGlob.sync('*.csproj', { cwd });
    if (!csprojFiles || csprojFiles.length === 0) {
        throw new Error('Unable to find *.csproj');
    }
    return csprojFiles[0];
}

function removeTrailingSeparator(s: string) {
    if (s.endsWith(path.sep)) {
        s = s.substring(0, s.length - path.sep.length);
    }
    return s;
}

function getPropertyFromProjectFile(filePath: string, propertyName: string) {
    let pattern = new RegExp(`<PropertyGroup>(?:.|\r|\n)*<${propertyName}>(.*)<\\/${propertyName}>(?:.|\r|\n)*</PropertyGroup>`, 'm');
    let match = pattern.exec(fs.readFileSync(filePath, 'utf8'));
    return (match && match[1]) || undefined;
}

function openTemplateAndSaveNewFile(templateName: string, context: any, newFilePath: string) {

    let templatePath = path.join(vscode.extensions.getExtension('mwaltersson.csharp-new-file')!.extensionPath, 'templates', templateName);

    vscode.workspace
        .openTextDocument(templatePath)
        .then(document => {
            let cursor = uuid();
            let template = handlebars.compile(document.getText());
            let text = template({ ...context, cursor });
            
            let cursorPosition = Math.max(0, text.indexOf(cursor));
            let textUpUntilCursor = text.substr(0, cursorPosition);
            
            text = text.replace(cursor, '');

            let line = textUpUntilCursor.split('\n').length - 1;
            let character = textUpUntilCursor.length - textUpUntilCursor.lastIndexOf('\n');

            fs.writeFileSync(newFilePath, text);

            return new vscode.Position(line, character);
        })
        .then(cursorPosition => {
            vscode.workspace.openTextDocument(newFilePath).then(document => {
                vscode.window.showTextDocument(document).then(editor => {
                    editor.selection = new vscode.Selection(cursorPosition, cursorPosition);
                });
            });
        })
        .then(undefined, (error: Error) => {
            vscode.window.showErrorMessage(`Error creating file from template: ${error && error.message}`);
        });
}
