# Change Log
All notable changes to the "csharp-new-file" extension will be documented in this file.

Check [Keep a Changelog](http://keepachangelog.com/) for recommendations on how to structure this file.

## [0.1.0]
- Initial release

## [0.2.0]

- Add `interface` and `enum` file templates

## [0.3.0]

- Resolve root namespace from `<RootNamespace>` or `<AssemblyName>` project file property

## [0.4.0]

- Add `record` and `struct` file templates and remove `namespace` indentation.
